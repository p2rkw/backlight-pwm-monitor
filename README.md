# README #

### What is this repository for? ###

On my laptop default backlight frequency (200 Mhz) causes screen flickering. This monitor will set desired PWM frequency on every resume or brightness change.

### How do I get set up? ###

```
git clone https://p2rkw@bitbucket.org/p2rkw/backlight-pwm-monitor.git
cd backlight-pwm-monitor/root
cp -r --parents * /
```


### To do ###

* Extract configuration (frequency, SYSFSLOC, LOGFILE) to separate file in /usr/local/etc
* Create installation/deinstallation/management script (preferably Makefile)

### Inspired by ###

* https://bbs.archlinux.org/viewtopic.php?pid=1245913
* http://devbraindom.blogspot.com/2013/03/eliminate-led-screen-flicker-with-intel.html
* http://www.x.org/docs/intel/VOL_3_display_registers.pdf

### Disclamer ###

Use at own risk