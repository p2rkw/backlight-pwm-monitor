#! /bin/bash

# About backlight PWM:
# http://devbraindom.blogspot.com/2013/03/eliminate-led-screen-flicker-with-intel.html

SYSFSLOC=${SYSFSLOC:=/sys/class/backlight/intel_backlight}

PIDFILE=${PIDFILE:=/var/run/set-pwm-2.pid}

LOGGER=${LOGGER:=/var/log/set-pwm-2.log}
function log(){
  echo "$(date  +'[%F %T]') $@" >> $LOGGER
}

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   log "This script must be run as root" 
   exit 1
fi

function decToHex(){
  printf "%.4x" "$@"
}

function getBrightness(){
  /bin/cat $SYSFSLOC/actual_brightness
}

function setBrightness(){
  echo "$1" > $SYSFSLOC/brightness
}

function unpackArgs(){
  local varList="$1"; shift
  echo "local $varList"
  for var in $(echo "$varList"); do
    echo "$var=\"$1\""
    shift
  done
}

function getPWMGranularity(){
# BLC_PWM_CPU_CTL2 is register at address 0x48250.
  intel_reg read BLC_PWM_CPU_CTL2 | grep -oP '(?<=granularity\s)(\d*)'
}

function getClockFreq(){
# 0xc6204 is address of RAWCLK_FREQ register.
# read more in doc (for HASWEL): https://01.org/sites/default/files/documentation/intel-gfx-prm-osrc-hsw-commandreference-registers_0.pdf
  local clockFreq=$(intel_reg read 0xc6204 | grep -oP '0x\d+$')
  echo $((clockFreq & 0x3ff))
}

function getPWM(){
# BLC_PWM_PCH_CTL2 is register at address 0x000c8254
  local regState=$(intel_reg read BLC_PWM_PCH_CTL2 | grep -oP '\(.*\)$')
  local freq=$(grep -oP '(?<=freq\s)\d+' <<< "$regState")
  local cycle=$(grep -oP '(?<=cycle\s)\d+' <<< "$regState")
  echo $freq $cycle
}

# convert:
# desired freq <-> register value
# using PCH display raw clocks multiplied by 128
function convertFreqReg(){
  local pwm="$1"
  /usr/bin/bc <<< "$(getClockFreq) * 1000000 / $(getPWMGranularity) / $pwm"
}

function writeFreqAndCycle(){
  local freq=${1}
  local cycle=${2}
  intel_reg write BLC_PWM_PCH_CTL2 "0x$(decToHex $freq)$(decToHex $cycle)"
}

function setNewPWM(){
  local newPwm="$(convertFreqReg $1)"
  local pwmGranularity=$(getPWMGranularity)
  local clockFreq=$(getClockFreq)
  eval "$(unpackArgs 'pwm cycle' $(getPWM))"
  if [[ $pwm -ne $newPwm ]]; then
    writeFreqAndCycle $newPwm $cycle
  fi
  log "Previous PWM freq: $pwm"
  log "New PWM freq: $newPwm"
}

function onChange(){
  sleep 2
#  echo onChange "$@"
  setNewPWM "$@"
}

function cleanup(){
  rm $PIDFILE
  exit 0
}

function monitor(){
  local pid level

  trap "cleanup" EXIT SIGINT SIGTERM
  echo "$$" > $PIDFILE

  onChange "$@" &
  pid=$!
  level=/sys/class/backlight/intel_backlight/

  inotifywait -m -q -e modify ${level}/actual_brightness ${level}/device/status |
    while read -r file event; do
      kill -9 $pid 2> /dev/null
      onChange "$@" &
      pid=$!
    done
}

function kill2(){
  if [[ -f $PIDFILE ]]; then
    pkill $(cat $PIDFILE)
    rm $PIDFILE
  fi
}

function main(){
  local i k
  local argsCount

  for ((i=0; i < $#; ++i)); do
    argsCount=0
    case "$1" in
    --monitor)
      argsCount=1
      kill2
      (monitor $2 &) &
    ;;
    --run-once)
      argsCount=1
      setNewPWM $2
    ;;
    --kill)
      argsCount=0
      kill2
    ;;
    --status)
      if [[ -f $PIDFILE ]]; then
        echo "enabled [$(cat $PIDFILE)]"
      else
        echo "disabled"
      fi
    ;;
    --trigger-monitor)
      argsCount=0
      setBrightness $(getBrightness)
    ;;
    --get-freq)
      argsCount=0
      local pwm cycle
      eval "$(unpackArgs 'pwm cycle' $(getPWM))"
      echo "$(convertFreqReg $pwm)"
    ;;
    *)
      echo "Unknown option '$1'" > /dev/stderr
      exit 1
    ;;
    esac

    # consume argument
    shift

    for ((k=0; k < argsCount; ++k)); do
      shift
    done
    argsCount=0

  done
}

main "$@"

